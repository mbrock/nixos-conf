{ config, lib, pkgs, ... }:

with lib;

let

  cfg = config.services.xserver.desktopManager.ratpoison;

in

{
  options = {

    services.xserver.desktopManager.ratpoison.enable = mkOption {
      default = true;
      example = false;
      description = "Enable ratpoison as a desktop manager.";
    };

  };

  config = mkIf (config.services.xserver.enable && cfg.enable) {

    services.xserver.desktopManager.session = singleton
      { name = "ratpoison";
        start = ''
          ${pkgs.ratpoison}/bin/ratpoison &
          waitPID=$!
        '';
      };

    environment.systemPackages = [ pkgs.ratpoison ];

  };

}
